package br.com.loja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.loja.fabrica.ConnectionFactory;
import br.com.loja.model.Compativel;


public class CompativelDao {
private Connection connection;
	
	public CompativelDao(){
		this.connection = new ConnectionFactory().getConnection();
 
	}
	
	public void adiciona(Compativel compativel){
		String sql = "INSERT into compativel" +
					 "(id_software,id_sistemas)" +
					 "values(?,?)";
		
		PreparedStatement stmt;
		try {
			stmt = connection.prepareStatement(sql);
			
			stmt.setInt(1,compativel.getId_software());
			stmt.setInt(2,compativel.getId_sistemas());

			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}

	
	public List<Compativel> buscarTodos(){
		try {
			String sql = "select * from compativel";
			List<Compativel> lista = new ArrayList<Compativel>();
			
			PreparedStatement stmt;
			stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				//Criando o Objeto do tipo Software para receber o resultSet.
				
				Compativel obj = new Compativel();
				obj.setId_sistemas(rs.getInt("id_sistemas"));
				obj.setId_software(rs.getInt("id_software"));
				
				lista.add(obj);
				
			}
			
			rs.close();
			stmt.close();
			return lista;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		
		
	}
	
}
