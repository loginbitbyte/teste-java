package br.com.loja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import br.com.loja.fabrica.ConnectionFactory;
import br.com.loja.model.Software;


public class SoftwareDao {
private Connection connection;
	
	public SoftwareDao(){
		this.connection = new ConnectionFactory().getConnection();
 
	}
	
	public void adiciona(Software software){
		String sql = "INSERT into software" +
					 "(versao,id_produto)" +
					 "values(?,?)";
		
		PreparedStatement stmt;
		try {
			stmt = connection.prepareStatement(sql);
			
			stmt.setInt(1,software.getVersao());
			stmt.setInt(2,software.getId_produto());

			
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	public List<Software> buscarTodos(){
		try {
			String sql = "select * from software";
			List<Software> lista = new ArrayList<Software>();
			
			PreparedStatement stmt;
			stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				//Criando o Objeto do tipo Software para receber o resultSet.
				
				Software obj = new Software();
				obj.setId(rs.getInt("id"));
				obj.setVersao(rs.getInt("versao"));
				obj.setId_produto(rs.getInt("id_produto"));
				
				lista.add(obj);
				
			}
			
			rs.close();
			stmt.close();
			return lista;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		
		
	}
	
	
	
}