package br.com.loja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.loja.fabrica.ConnectionFactory;
import br.com.loja.model.Sistemas;


public class SistemasDao {

private Connection connection;
	
	public SistemasDao(){
		this.connection = new ConnectionFactory().getConnection();
 
	}
	
	public void adiciona(Sistemas sistemas){
		String sql = "INSERT into sistemas" +
					 "(nome,ano,versao)" +
					 "values(?,?,?)";
		
		PreparedStatement stmt;
		try {
			stmt = connection.prepareStatement(sql);
			
			stmt.setString(1,sistemas.getNome());
			stmt.setInt(2,sistemas.getAno());
			stmt.setString(3, sistemas.getVersao());
			
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	
	public List<Sistemas> buscarTodos(){
		try {
			String sql = "select * from sistemas";
			List<Sistemas> lista = new ArrayList<Sistemas>();
			
			PreparedStatement stmt;
			stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				//Criando o Objeto do tipo Software para receber o resultSet.
				
				Sistemas obj = new Sistemas();
				obj.setId(rs.getInt("id"));
				obj.setNome(rs.getString("nome"));
				obj.setAno(rs.getInt("ano"));
				obj.setVersao(rs.getString("versao"));
				
				
				lista.add(obj);
				
			}
			
			rs.close();
			stmt.close();
			return lista;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		
		
	}

}
