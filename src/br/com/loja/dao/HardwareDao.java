package br.com.loja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.loja.fabrica.ConnectionFactory;
import br.com.loja.model.Hardware;


public class HardwareDao {
	
private Connection connection;
	
	public HardwareDao(){
		this.connection = new ConnectionFactory().getConnection();
 
	}
	
	public void adiciona(Hardware hardware){
		String sql = "INSERT into hardware" +
					 "(modelo,fabricante,id_produto)" +
					 "values(?,?,?)";
		
		PreparedStatement stmt;
		try {
			stmt = connection.prepareStatement(sql);
			
			stmt.setString(1,hardware.getModelo());
			stmt.setString(2,hardware.getFabricante());
			stmt.setInt(3, hardware.getId_produto());
			
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	
	public List<Hardware> buscarTodos(){
		try {
			String sql = "select * from hardware";
			List<Hardware> lista = new ArrayList<Hardware>();
			
			PreparedStatement stmt;
			stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				//Criando o Objeto do tipo Software para receber o resultSet.
				
				Hardware obj = new Hardware();
				obj.setId(rs.getInt("id"));
				obj.setModelo(rs.getString("modelo"));
				obj.setFabricante(rs.getString("fabricante"));
				obj.setId_produto(rs.getInt("id_produto"));
				
				
				lista.add(obj);
				
			}
			
			rs.close();
			stmt.close();
			return lista;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		
		
	}

	
}
