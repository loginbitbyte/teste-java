package br.com.loja.model;

public class Compativel {

	private Integer id_software;
	private Integer id_sistema;
	
	
	public Integer getId_software() {
		return id_software;
	}
	public void setId_software(Integer id_software) {
		this.id_software = id_software;
	}
	public Integer getId_sistemas() {
		return id_sistema;
	}
	public void setId_sistemas(Integer id_sistema) {
		this.id_sistema = id_sistema;
	}	
	
}
